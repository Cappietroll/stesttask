from .views import cut_url, redirect


def setup_routes(app):
    app.router.add_post('/', cut_url)
    app.router.add_get(r'/{uid:\d+}', redirect)
