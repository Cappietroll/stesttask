from .init_db import init_db

from aiohttp import web
from .routes import setup_routes
from .db import init_aiopg, close_aiopg

init_db()
app = web.Application()
setup_routes(app)
app.on_startup.append(init_aiopg)
app.on_cleanup.append(close_aiopg)
web.run_app(app)
