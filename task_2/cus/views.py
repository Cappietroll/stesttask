import validators

from aiohttp import web
from .db import get_or_create_url_from_db, get_original_url_from_db


async def cut_url(request):
    if request.query.get('url_to_redirect'):
        url_to_redirect = request.query['url_to_redirect']
        if validators.url(url_to_redirect):
            uid = await get_or_create_url_from_db(
                url_to_redirect, request.app['db'])
            return web.json_response(
                {'short_url': request.scheme+'://'+request.host+'/'+str(uid)}
            )
        raise web.HTTPBadRequest(text="Can't validate url")
    raise web.HTTPNoContent


async def redirect(request):
    uid = request.match_info.get('uid')
    if uid:
        url_to_redirect = await get_original_url_from_db(
            uid, request.app['db'])
        if url_to_redirect:
            raise web.HTTPFound(url_to_redirect)
    raise web.HTTPNotFound
