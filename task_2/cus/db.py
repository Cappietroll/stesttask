from aiopg.sa import create_engine
from sqlalchemy import (
    MetaData, Table, Column,
    Integer, Text
)

from .settings import (
    POSTGRES_HOST, POSTGRES_PORT,
    POSTGRES_USER, POSTGRES_PASS,
    POSTGRES_DB)


meta = MetaData()

urls = Table(
    'urls', meta,
    Column('uid', Integer, primary_key=True, autoincrement=True),
    Column('retrieve_url', Text, nullable=False, unique=True),
)


async def init_aiopg(app):
    engine = await create_engine(
        database=POSTGRES_DB,
        user=POSTGRES_USER,
        password=POSTGRES_PASS,
        host=POSTGRES_HOST,
        port=POSTGRES_PORT,
    )
    app['db'] = engine


async def close_aiopg(app):
    app['db'].close()
    await app['db'].wait_closed()


async def get_query_result(query, conn):
    cur = await conn.execute(query)
    result = await cur.fetchone()
    if result:
        return result[0]


async def get_original_url_from_db(uid, db):
    async with db.acquire() as conn:
        select_query = f'''
            select
                retrieve_url
            from
                urls
            where uid={uid}
        '''
        return await get_query_result(select_query, conn)


async def get_or_create_url_from_db(url, db):
    async with db.acquire() as conn:
        insert_query = f"""
            insert into
                urls (retrieve_url)
            values
                ('{url}')
            on conflict (retrieve_url)
                do nothing
            RETURNING uid;"""

        select_query = f"""
            select
                uid
            from
                urls
            where retrieve_url='{url}'
        """

        already_exist_uid = await get_query_result(select_query, conn)
        if already_exist_uid:
            return already_exist_uid
        return await get_query_result(insert_query, conn)
