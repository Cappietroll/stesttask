from sqlalchemy import create_engine, MetaData

from cus.settings import POSTGRES_DSN
from cus.db import urls


def create_tables(engine):
    meta = MetaData()
    meta.create_all(bind=engine, tables=[urls])


def init_db():
    engine = create_engine(POSTGRES_DSN)
    create_tables(engine)
