
Я сделал два варианта так как если рассматривать задачу в вакуме и жестко придерживаться принципа DRY 
то первый вариант предпочтительней.
Однако если рассматривать задачу ближе к реальности то цена, вполне себе, может вычисляется у Order и Invoice по-разному.
Это будет заметно когда добавится бизнес-логика. 
Единственое что их объединяет - проверка на отрицательное значение. 
Поэтому я решил не делать миксин для обоих классов а решил сделать два независимых класса и декоратор для проверки 
вносимого значения атрибута.