class PriceValueCheckerMixin:
    def __init__(self):
        self._price = None

    @property
    def price(self):
        return self._price

    @price.setter
    def price(self, value):
        if (not isinstance(value, (int, float))
                or isinstance(value, (int, float)) and value < 0):
            raise AttributeError
        self._price = value


class Order(PriceValueCheckerMixin):
    pass


class Invoce(PriceValueCheckerMixin):
    pass

