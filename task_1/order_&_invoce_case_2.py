class PaymentProcessingError(Exception):
    pass


def forbid_negative_price(func):
    def wrapper(*args, **kwargs):
        if (not isinstance(args[1], (int, float))
                or isinstance(args[1], (int, float)) and args[1] < 0):
            raise PaymentProcessingError('Price can not be negative')
        return func(*args, **kwargs)
    return wrapper


class Order:
    def __init__(self):
        self._price = None

    @property
    def price(self):
        return self._price

    @price.setter
    @forbid_negative_price
    def price(self, value):
        # здесь какая-то бизнес-логика  для order
        self._price = value


class Invoice:
    def __init__(self):
        self._price = None

    @property
    def price(self):
        return self._price

    @price.setter
    @forbid_negative_price
    def price(self, value):
        # здесь какая-то бизнес-логика  для invoice
        self._price = value
